package com.astra.life.employees.controller;

import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.request.CreateDepartmentsRequest;
import com.astra.life.employees.request.UpdateDepartmentRequest;
import com.astra.life.employees.response.ApiResponse;
import com.astra.life.employees.response.DepartmentResponse;
import com.astra.life.employees.service.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/department")
public class DepartmentController {

    DepartmentService departmentService;

    @GetMapping("/list")
    public ApiResponse<?> getAllDepartment() throws RecordNotFoundException {
        List<DepartmentResponse> departmentResponses;

        departmentResponses = departmentService.getAllDepartment();

        return ApiResponse.success(departmentResponses);
    }

    @GetMapping("/detail")
    public ApiResponse<?> getDepartmentByDeptNo(@RequestParam String deptNo ) throws RecordNotFoundException{
        DepartmentResponse departmentResponse = new DepartmentResponse();
        departmentResponse =  departmentService.getDepartmentByDeptNo(deptNo);
        return ApiResponse.success(departmentResponse);
    }

    @PostMapping("/create")
    public ApiResponse<?> createDepartment(@RequestBody @Valid CreateDepartmentsRequest createDepartmentsRequest) throws RecordNotFoundException {

        DepartmentResponse response = departmentService.createDepartment(createDepartmentsRequest);
        return ApiResponse.success(response);
    }

    @PutMapping("/update")
    public ApiResponse<?> updateEmployee(@RequestBody @Valid UpdateDepartmentRequest updateDepartmentRequest) throws RecordNotFoundException {

        DepartmentResponse response = departmentService.updateDepartment(updateDepartmentRequest);
        return ApiResponse.success(response);
    }

    @DeleteMapping("/delete")
    public ApiResponse<?> deleteDepartment(@RequestParam String deptNo ) throws RecordNotFoundException {

        String response = departmentService.deleteDepartment(deptNo);
        return ApiResponse.success(response);
    }
}
