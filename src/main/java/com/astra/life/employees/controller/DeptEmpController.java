package com.astra.life.employees.controller;

import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.request.CreateDeptEmpRequest;
import com.astra.life.employees.response.ApiResponse;
import com.astra.life.employees.response.DeptEmpResponse;
import com.astra.life.employees.service.DeptEmpService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/deptemp")
public class DeptEmpController {

    DeptEmpService deptEmpService;

    @PostMapping("/create")
    public ApiResponse<?> createDepartmentEmp(@RequestBody @Valid CreateDeptEmpRequest createDeptEmpRequest) throws RecordNotFoundException {

        DeptEmpResponse response = deptEmpService.createDeptEmp(createDeptEmpRequest);
        return ApiResponse.success(response);
    }

    @DeleteMapping("/delete")
    public ApiResponse<?> deleteDepartmentEmp(@RequestParam Integer empNo ,@RequestParam String deptNo ) throws RecordNotFoundException {

        String response = deptEmpService.delete(empNo, deptNo);
        return ApiResponse.success(response);
    }

    @GetMapping("/detail")
    public ApiResponse<?> getDepartmentEmpByEmpNo(@RequestParam Integer empNo ) throws RecordNotFoundException{
        DeptEmpResponse deptEmpResponse = new DeptEmpResponse();
        deptEmpResponse =  deptEmpService.getDeptEmpByEmpNo(empNo);
        return ApiResponse.success(deptEmpResponse);
    }
}
