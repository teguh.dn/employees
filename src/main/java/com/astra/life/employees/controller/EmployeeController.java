package com.astra.life.employees.controller;

import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.request.CreateEmployeeRequest;
import com.astra.life.employees.request.UpdateEmployeeRequest;
import com.astra.life.employees.response.ApiResponse;
import com.astra.life.employees.response.EmployeesResponse;
import com.astra.life.employees.service.EmployeesService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/employee")
public class EmployeeController {

    EmployeesService employeesService;

    @GetMapping("/list")
    public ApiResponse<?> getAllEmployee() throws RecordNotFoundException {
        List<EmployeesResponse> listEmployee;

        listEmployee = employeesService.getAllEmployee();

        return ApiResponse.success(listEmployee);
    }

    @GetMapping("/detail")
    public ApiResponse<?> getEmployeeByEmpNo(@RequestParam Integer empNo ) throws RecordNotFoundException{
        EmployeesResponse employeesResponse = new EmployeesResponse();
        employeesResponse =  employeesService.getEmployeeByEmpNo(empNo);
        return ApiResponse.success(employeesResponse);
    }

    @PostMapping("/create")
    public ApiResponse<?> createEmployee(@RequestBody @Valid CreateEmployeeRequest createEmployeeRequest) throws RecordNotFoundException {

        EmployeesResponse response = employeesService.create(createEmployeeRequest);
        return ApiResponse.success(response);
    }

    @PutMapping("/update")
    public ApiResponse<?> updateEmployee(@RequestBody @Valid UpdateEmployeeRequest updateEmployeeRequest) throws RecordNotFoundException {

        EmployeesResponse response = employeesService.update(updateEmployeeRequest);
        return ApiResponse.success(response);
    }

    @DeleteMapping("/delete")
    public ApiResponse<?> deleteEmployee(@RequestParam Integer empNo ) throws RecordNotFoundException {

        String response = employeesService.delete(empNo);
        return ApiResponse.success(response);
    }

}
