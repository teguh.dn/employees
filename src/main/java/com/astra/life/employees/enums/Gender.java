package com.astra.life.employees.enums;

public enum Gender {
    M("M"), F("F");

    public String getGender() {
        return gender;
    }

    private final String gender;

    Gender(String gender) {
        this.gender = gender;
    }
}
