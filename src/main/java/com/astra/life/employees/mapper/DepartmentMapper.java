package com.astra.life.employees.mapper;

import com.astra.life.employees.model.Departments;
import com.astra.life.employees.request.CreateDepartmentsRequest;
import com.astra.life.employees.request.UpdateDepartmentRequest;
import com.astra.life.employees.response.DepartmentResponse;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class DepartmentMapper {

    public abstract DepartmentResponse convertToResponse(Departments departments);

    @BeanMapping(qualifiedByName = "create")
    public abstract Departments toModel(@MappingTarget Departments departments, CreateDepartmentsRequest request);


    public abstract Departments toModelValue(@MappingTarget Departments departments, DepartmentResponse response);

    @BeanMapping(qualifiedByName = "update")
    public abstract Departments toModelUpdate(@MappingTarget Departments departments, UpdateDepartmentRequest request);
}
