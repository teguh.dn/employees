package com.astra.life.employees.mapper;

import com.astra.life.employees.model.DeptEmp;
import com.astra.life.employees.request.CreateDeptEmpRequest;
import com.astra.life.employees.request.UpdateDepartmentRequest;
import com.astra.life.employees.response.DeptEmpResponse;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class DeptEmpMapper {
    @Mappings({
            @Mapping(source = "employees.empNo", target = "empNo"),
            @Mapping(source = "departments.deptNo", target = "deptNo"),
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptEmpResponse convertToResponse(DeptEmp deptEmp);

    @BeanMapping(qualifiedByName = "create")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptEmp toModel(@MappingTarget DeptEmp deptEmp, CreateDeptEmpRequest request);

    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptEmp toModelValue(@MappingTarget DeptEmp deptEmp, DeptEmpResponse response);

    @BeanMapping(qualifiedByName = "update")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptEmp toModelUpdate(@MappingTarget DeptEmp deptEmp, CreateDeptEmpRequest request);
}
