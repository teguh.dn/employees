package com.astra.life.employees.mapper;

import com.astra.life.employees.model.DeptManager;
import com.astra.life.employees.request.CreateDeptManagerRequest;
import com.astra.life.employees.response.DeptManagerResponse;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class DeptManagerMapper {
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptManagerResponse convertToResponse(DeptManager deptManager);

    @BeanMapping(qualifiedByName = "create")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptManager toModel(@MappingTarget DeptManager deptManager, CreateDeptManagerRequest request);

    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptManager toModelValue(@MappingTarget DeptManager deptManager, DeptManagerResponse response);

    @BeanMapping(qualifiedByName = "update")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract DeptManager toModelUpdate(@MappingTarget DeptManager deptManager, CreateDeptManagerRequest request);
}
