package com.astra.life.employees.mapper;

import com.astra.life.employees.model.Employees;
import com.astra.life.employees.request.CreateEmployeeRequest;
import com.astra.life.employees.request.UpdateEmployeeRequest;
import com.astra.life.employees.response.EmployeesResponse;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class EmployeeMapper {

    @Mappings({
            @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "hireDate", target = "hireDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract EmployeesResponse convertToResponse(Employees employees);

    @BeanMapping(qualifiedByName = "create")
    @Mappings({
            @Mapping(target = "empNo", ignore = true),
            @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "hireDate", target = "hireDate", dateFormat = "yyyy-MM-dd")
    })
    public abstract Employees toModel(@MappingTarget Employees employees, CreateEmployeeRequest request);

    @Mappings({
            @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "hireDate", target = "hireDate", dateFormat = "yyyy-MM-dd")
    })
    public abstract Employees toModelValue(@MappingTarget Employees employees, EmployeesResponse response);

    @BeanMapping(qualifiedByName = "update")
    @Mappings({
            @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "hireDate", target = "hireDate", dateFormat = "yyyy-MM-dd")
    })
    public abstract Employees toModelUpdate(@MappingTarget Employees employees, UpdateEmployeeRequest request);
}
