package com.astra.life.employees.mapper;

import com.astra.life.employees.model.Salaries;
import com.astra.life.employees.request.CreateSalariesRequest;
import com.astra.life.employees.response.SalariesResponse;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class SalariesMapper {
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract SalariesResponse convertToResponse(Salaries salaries);

    @BeanMapping(qualifiedByName = "create")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract Salaries toModel(@MappingTarget Salaries salaries, CreateSalariesRequest request);

    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract Salaries toModelValue(@MappingTarget Salaries salaries, SalariesResponse response);

    @BeanMapping(qualifiedByName = "update")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract Salaries toModelUpdate(@MappingTarget Salaries salaries, CreateSalariesRequest request);
}
