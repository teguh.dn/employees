package com.astra.life.employees.mapper;

import com.astra.life.employees.model.Titles;
import com.astra.life.employees.request.CreateTitlesRequest;
import com.astra.life.employees.response.TitlesResponse;
import org.mapstruct.*;

@Mapper(componentModel = "spring")
public abstract class TitlesMapper {
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract TitlesResponse convertToResponse(Titles titles);

    @BeanMapping(qualifiedByName = "create")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract Titles toModel(@MappingTarget Titles titles, CreateTitlesRequest request);

    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract Titles toModelValue(@MappingTarget Titles titles, TitlesResponse response);

    @BeanMapping(qualifiedByName = "update")
    @Mappings({
            @Mapping(source = "fromDate", target = "fromDate", dateFormat = "yyyy-MM-dd"),
            @Mapping(source = "toDate", target = "toDate", dateFormat = "yyyy-MM-dd"),
    })
    public abstract Titles toModelUpdate(@MappingTarget Titles titles, CreateTitlesRequest request);
}
