package com.astra.life.employees.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "departments")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Departments {

    @Id
    @Column(name="dept_no", columnDefinition = "CHAR(4)")
    private String deptNo;

    @Column(name="dept_name", columnDefinition = "VARCHAR(40)")
    private String deptName;
}
