package com.astra.life.employees.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "dept_emp")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@IdClass(DeptEmpKey.class)
public class DeptEmp{

    @Id
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "emp_no", referencedColumnName = "emp_no" , insertable = false, updatable = false)
    private Employees employees;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "dept_no", referencedColumnName = "dept_no", insertable = false, updatable = false)
    private Departments departments;

    @Column(name="from_date", columnDefinition = "DATE", nullable = false)
    private LocalDate fromDate;

    @Column(name="to_date", columnDefinition = "DATE", nullable = false)
    private LocalDate toDate;
}
