package com.astra.life.employees.model;

import java.io.Serializable;

public class DeptEmpKey implements Serializable {
    private Integer employees;
    private String departments;

    public DeptEmpKey(){

    }

    public DeptEmpKey(Integer employees, String departments){
        this.employees = employees;
        this.departments =departments;
    }
    @Override
    public int hashCode(){
        return super.hashCode();
    }
    @Override
    public boolean equals(Object object){
        return super.equals(object);
    }
}
