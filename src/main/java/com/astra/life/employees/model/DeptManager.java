package com.astra.life.employees.model;


import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "dept_manager")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@IdClass(DeptManagerKey.class)
public class DeptManager {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_no", referencedColumnName = "emp_no")
    private Employees employees;

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dept_no", referencedColumnName = "dept_no")
    private Departments departments;

    @Column(name="from_date", columnDefinition = "DATE", nullable = false)
    private LocalDate fromDate;

    @Column(name="to_date", columnDefinition = "DATE", nullable = false)
    private LocalDate toDate;
}
