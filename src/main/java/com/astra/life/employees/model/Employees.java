package com.astra.life.employees.model;

import com.astra.life.employees.enums.Gender;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;


@Entity
@Table(name = "employees")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Employees {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="emp_no")
    private Integer empNo;

    @Column(name="birth_date", columnDefinition = "DATE")
    private LocalDate birthDate;

    @Column(name="first_name", columnDefinition = "VARCHAR(14)")
    private String firstName;

    @Column(name="last_name", columnDefinition = "VARCHAR(16)")
    private String lastName;

    @Enumerated(EnumType.STRING)
    @Column(name="gender", columnDefinition = "ENUM")
    private Gender gender;

//    @Column(name="gender", columnDefinition = "ENUM")
//    private Enum gender;

    @Column(name="hire_date", columnDefinition = "DATE")
    private LocalDate hireDate;

}
