package com.astra.life.employees.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "salaries")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@IdClass(SalariesKey.class)
public class Salaries {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_no", referencedColumnName = "emp_no")
    private Employees employees;

    @Column(name="salary", columnDefinition = "Integer")
    private Integer salary;

    @Id
    @Column(name="from_date", columnDefinition = "DATE", nullable = false)
    private LocalDate fromDate;

    @Column(name="to_date", columnDefinition = "DATE", nullable = false)
    private LocalDate toDate;
}
