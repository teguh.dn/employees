package com.astra.life.employees.model;

import java.io.Serializable;

public class SalariesKey implements Serializable {
    private Integer employees;

    public SalariesKey(){

    }
    public SalariesKey(Integer employees){
        this.employees = employees;
    }
    @Override
    public int hashCode(){
        return super.hashCode();
    }
    @Override
    public boolean equals(Object object){
        return super.equals(object);
    }
}
