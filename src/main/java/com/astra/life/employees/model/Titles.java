package com.astra.life.employees.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "titles")
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@IdClass(TitleKey.class)
public class Titles {

    @Id
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_no", referencedColumnName = "emp_no")
    private Employees employees;

    @Id
    @Column(name="title", columnDefinition = "VARCHAR(50)")
    private String title;

    @Id
    @Column(name="from_date", columnDefinition = "DATE", nullable = false)
    private LocalDate fromDate;

    @Column(name="to_date", columnDefinition = "DATE", nullable = false)
    private LocalDate toDate;
}
