package com.astra.life.employees.repository;

import com.astra.life.employees.model.Departments;
import org.springframework.data.repository.CrudRepository;

public interface DepartmentsRepository extends CrudRepository<Departments, Long> {
    Departments findByDeptNo(String deptNo);
}
