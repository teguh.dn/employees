package com.astra.life.employees.repository;

import com.astra.life.employees.model.DeptEmp;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

public interface DeptEmpRepository extends CrudRepository<DeptEmp, Long> {
    DeptEmp findDistinctByDepartments_DeptNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(String deptNo, LocalDate now, LocalDate now1);

    DeptEmp findDistinctByEmployees_EmpNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(Integer empNo, LocalDate now, LocalDate now1);

    List<DeptEmp> findByEmployees_EmpNoAndDepartments_DeptNo(Integer empNo, String deptNo);
}
