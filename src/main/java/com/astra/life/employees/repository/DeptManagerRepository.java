package com.astra.life.employees.repository;

import com.astra.life.employees.model.DeptManager;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;
import java.util.List;

public interface DeptManagerRepository extends CrudRepository<DeptManager, Long> {

    DeptManager findDistinctByDepartments_DeptNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(String deptNo, LocalDate now, LocalDate now1);

    DeptManager findDistinctByEmployees_EmpNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(Integer empNo, LocalDate now, LocalDate now1);

    List<DeptManager> findByEmployees_EmpNoAndDepartments_DeptNo(Integer empNo, String deptNo);
}
