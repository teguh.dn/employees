package com.astra.life.employees.repository;

import com.astra.life.employees.model.Employees;
import org.springframework.data.repository.CrudRepository;


import java.util.Optional;

public interface EmployeesRepository extends CrudRepository<com.astra.life.employees.model.Employees, Long> {
    Employees findByEmpNo(Integer empNo);

    Optional<Employees> findAllByFirstNameContains(String name);
}
