package com.astra.life.employees.repository;

import com.astra.life.employees.model.Salaries;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface SalariesRepository extends CrudRepository<Salaries, Long> {

    Salaries findDistinctByEmployees_EmpNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(Integer empNo, LocalDate now, LocalDate now1);
}
