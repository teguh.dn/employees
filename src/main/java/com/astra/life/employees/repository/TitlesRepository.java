package com.astra.life.employees.repository;

import com.astra.life.employees.model.Titles;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface TitlesRepository extends CrudRepository<Titles, Long> {

    Titles findDistinctByEmployees_EmpNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(Integer empNo, LocalDate now, LocalDate now1);
}
