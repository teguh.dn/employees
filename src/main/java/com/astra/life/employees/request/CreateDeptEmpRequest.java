package com.astra.life.employees.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateDeptEmpRequest {

    @NotNull
    private Integer empNo;

    @NotNull
    @NotBlank
    private String deptNo;

    @NotNull
    @NotBlank
    private String fromDate;

    @NotNull
    @NotBlank
    private String toDate;
}
