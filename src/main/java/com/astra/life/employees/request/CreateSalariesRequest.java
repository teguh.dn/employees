package com.astra.life.employees.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class CreateSalariesRequest {

    @NotNull
    private Integer empNo;

    @NotNull
    private Integer salary;

    @NotNull
    @NotBlank
    private String fromDate;

    @NotNull
    @NotBlank
    private String toDate;
}
