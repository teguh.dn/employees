package com.astra.life.employees.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UpdateDepartmentRequest {

    @NotNull
    private String deptNo;

    @NotNull
    @NotBlank
    private String deptName;
}
