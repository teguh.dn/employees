package com.astra.life.employees.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class UpdateEmployeeRequest {
    @NotNull
    private Integer empNo;

    @NotNull
    @NotBlank
    private String birthDate;

    @NotNull
    @NotBlank
    private String firstName;

    @NotNull
    @NotBlank
    private String lastName;

    @NotNull
    @NotBlank
    private String gender;

    @NotNull
    @NotBlank
    private String hireDate;
}
