package com.astra.life.employees.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DepartmentResponse {

    private String deptNo;
    private String deptName;

}
