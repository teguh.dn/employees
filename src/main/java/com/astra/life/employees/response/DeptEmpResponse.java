package com.astra.life.employees.response;

import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeptEmpResponse {

    private String deptNo;
    private Integer empNo;
    private String fromDate;
    private String toDate;

}
