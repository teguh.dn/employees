package com.astra.life.employees.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DeptManagerResponse {

    private String deptNo;
    private Integer empNo;
    private String fromDate;
    private String toDate;
}
