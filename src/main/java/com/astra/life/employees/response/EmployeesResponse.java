package com.astra.life.employees.response;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class EmployeesResponse {

    private Integer empNo;
    private String firstName;
    private String lastName;
    private String gender;
    private String birthDate;
    private String hireDate;
}
