package com.astra.life.employees.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SalariesResponse {

    private Integer empNo;
    private Integer salary;
    private String fromDate;
    private String toDate;

}
