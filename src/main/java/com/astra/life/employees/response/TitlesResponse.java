package com.astra.life.employees.response;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TitlesResponse {

    private Integer empNo;
    private String title;
    private String fromDate;
    private String toDate;
}
