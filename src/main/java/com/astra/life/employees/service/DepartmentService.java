package com.astra.life.employees.service;

import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.request.CreateDepartmentsRequest;
import com.astra.life.employees.request.UpdateDepartmentRequest;
import com.astra.life.employees.response.DepartmentResponse;

import java.util.List;

public interface DepartmentService {

    List<DepartmentResponse> getAllDepartment() throws RecordNotFoundException;

    DepartmentResponse getDepartmentByDeptNo(String deptNo) throws RecordNotFoundException;

    DepartmentResponse createDepartment(CreateDepartmentsRequest createDepartmentsRequest) throws RecordNotFoundException;

    DepartmentResponse updateDepartment(UpdateDepartmentRequest updateDepartmentRequest) throws RecordNotFoundException;

    String deleteDepartment(String deptNo) throws RecordNotFoundException;


}
