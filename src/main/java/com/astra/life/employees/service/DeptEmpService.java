package com.astra.life.employees.service;

import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.request.CreateDeptEmpRequest;
import com.astra.life.employees.response.DeptEmpResponse;

public interface DeptEmpService {

    DeptEmpResponse getDeptEmpByDeptNo(String deptNo) throws RecordNotFoundException;

    DeptEmpResponse getDeptEmpByEmpNo(Integer empNo) throws RecordNotFoundException;

    DeptEmpResponse createDeptEmp(CreateDeptEmpRequest request) throws RecordNotFoundException;

    DeptEmpResponse updateDeptEmp(CreateDeptEmpRequest request) throws RecordNotFoundException;

    String delete(Integer empNo, String deptNo) throws RecordNotFoundException;

}
