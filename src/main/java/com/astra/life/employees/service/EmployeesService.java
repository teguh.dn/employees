package com.astra.life.employees.service;

import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.request.CreateEmployeeRequest;
import com.astra.life.employees.request.UpdateEmployeeRequest;
import com.astra.life.employees.response.EmployeesResponse;

import java.util.List;

public interface EmployeesService {
    List<EmployeesResponse> getAllEmployee() throws RecordNotFoundException;

    EmployeesResponse getEmployeeByEmpNo(Integer empNo) throws RecordNotFoundException;

    EmployeesResponse create(CreateEmployeeRequest createEmployeeRequest) throws RecordNotFoundException;

    EmployeesResponse update(UpdateEmployeeRequest updateEmployeeRequest) throws RecordNotFoundException;

    String delete(Integer empNo) throws RecordNotFoundException;

}
