package com.astra.life.employees.service.impl;

import com.astra.life.employees.enums.ResponseCode;
import com.astra.life.employees.enums.ResponseMessage;
import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.mapper.DepartmentMapper;
import com.astra.life.employees.model.Departments;
import com.astra.life.employees.repository.DepartmentsRepository;
import com.astra.life.employees.request.CreateDepartmentsRequest;
import com.astra.life.employees.request.UpdateDepartmentRequest;
import com.astra.life.employees.response.DepartmentResponse;
import com.astra.life.employees.service.DepartmentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {
    private final DepartmentsRepository departmentsRepository;
    private final DepartmentMapper departmentMapper;


    public List<DepartmentResponse> getAllDepartment() throws RecordNotFoundException {
        List<DepartmentResponse> responseList = new ArrayList<>();
        try {
            Iterable<Departments> departments = departmentsRepository.findAll();
            for(Departments dept : departments){
                responseList.add(departmentMapper.convertToResponse(dept));
            }
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return  responseList;
    }

    @Override
    public DepartmentResponse getDepartmentByDeptNo(String deptNo) throws RecordNotFoundException{
        DepartmentResponse DepartmentResponse = new DepartmentResponse();
        try {
            Departments departments =  departmentsRepository.findByDeptNo(deptNo);
            DepartmentResponse = departmentMapper.convertToResponse(departments);
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return  DepartmentResponse;
    }

    @Override
    public DepartmentResponse createDepartment(CreateDepartmentsRequest createDepartmentsRequest) throws RecordNotFoundException {
        Departments departments = new Departments();
        DepartmentResponse response = new DepartmentResponse();
        departmentMapper.toModel(departments, createDepartmentsRequest);

        try{
            departments = departmentsRepository.save(departments);
            response = departmentMapper.convertToResponse(departments);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }

    @Override
    public DepartmentResponse updateDepartment(UpdateDepartmentRequest updateDepartmentRequest) throws RecordNotFoundException {
        Departments departments = new Departments();
        Departments dept = new Departments();
        DepartmentResponse response = new DepartmentResponse();
        departmentMapper.toModelUpdate(departments, updateDepartmentRequest);
        try{
            departments = departmentsRepository.save(departments);
            response = departmentMapper.convertToResponse(departments);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }

    @Override
    public String deleteDepartment(String deptNo) throws  RecordNotFoundException{
        String result;
        try {
            Departments departments = departmentsRepository.findByDeptNo(deptNo);
            if (departments == null)throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), "Employee not Found");
            departmentsRepository.delete(departments);
            result = "Data has been deleted";
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_DELETE.getMessage());
        }
        return result;
    }
    
}
