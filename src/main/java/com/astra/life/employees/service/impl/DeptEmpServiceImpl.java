package com.astra.life.employees.service.impl;

import com.astra.life.employees.enums.ResponseCode;
import com.astra.life.employees.enums.ResponseMessage;
import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.mapper.DeptEmpMapper;
import com.astra.life.employees.model.Departments;
import com.astra.life.employees.model.DeptEmp;
import com.astra.life.employees.model.Employees;
import com.astra.life.employees.repository.DepartmentsRepository;
import com.astra.life.employees.repository.DeptEmpRepository;
import com.astra.life.employees.repository.EmployeesRepository;
import com.astra.life.employees.request.CreateDeptEmpRequest;
import com.astra.life.employees.response.DeptEmpResponse;
import com.astra.life.employees.service.DeptEmpService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Service
@AllArgsConstructor
public class DeptEmpServiceImpl implements DeptEmpService {

    private final DeptEmpRepository deptEmpRepository;
    private final DeptEmpMapper deptEmpMapper;
    private final EmployeesRepository employeesRepository;
    private final DepartmentsRepository departmentsRepository;

    @Override
    public DeptEmpResponse getDeptEmpByDeptNo(String deptNo) throws RecordNotFoundException{
        DeptEmpResponse deptEmpResponse = new DeptEmpResponse();
        try {
            LocalDate now = LocalDate.now();
            DeptEmp deptEmp = deptEmpRepository.findDistinctByDepartments_DeptNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(deptNo, now, now);
            deptEmpResponse = deptEmpMapper.convertToResponse(deptEmp);
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return deptEmpResponse;
    }

    @Override
    public DeptEmpResponse getDeptEmpByEmpNo(Integer empNo) throws RecordNotFoundException{
        DeptEmpResponse deptEmpResponse = new DeptEmpResponse();
        try {
            LocalDate now = LocalDate.now();
            DeptEmp deptEmp = deptEmpRepository.findDistinctByEmployees_EmpNoAndFromDateLessThanEqualAndToDateGreaterThanEqual(empNo, now, now);
            deptEmpResponse = deptEmpMapper.convertToResponse(deptEmp);
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return deptEmpResponse;
    }

    @Override
    public DeptEmpResponse createDeptEmp(CreateDeptEmpRequest createDeptEmpRequest) throws RecordNotFoundException {
        DeptEmp deptEmp = new DeptEmp();
        DeptEmpResponse response = new DeptEmpResponse();
        deptEmpMapper.toModel(deptEmp, createDeptEmpRequest);

        try{
            Employees employee = employeesRepository.findByEmpNo(createDeptEmpRequest.getEmpNo());
            Departments departments = departmentsRepository.findByDeptNo(createDeptEmpRequest.getDeptNo());
            deptEmp.setEmployees(employee);
            deptEmp.setDepartments(departments);
            deptEmp = deptEmpRepository.save(deptEmp);
            response = deptEmpMapper.convertToResponse(deptEmp);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }

    @Override
    public DeptEmpResponse updateDeptEmp(CreateDeptEmpRequest createDeptEmpRequest) throws RecordNotFoundException {
        DeptEmp deptEmp = new DeptEmp();
        DeptEmpResponse response = new DeptEmpResponse();
        deptEmpMapper.toModelUpdate(deptEmp, createDeptEmpRequest);

        try{
            deptEmp = deptEmpRepository.save(deptEmp);
            response = deptEmpMapper.convertToResponse(deptEmp);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }

    @Override
    public String delete(Integer empNo, String deptNo) throws  RecordNotFoundException{
        String result;
        try {
            List<DeptEmp> deptEmp = deptEmpRepository.findByEmployees_EmpNoAndDepartments_DeptNo(empNo, deptNo);
            if(deptEmp != null){
                for(DeptEmp deptEmp1 : deptEmp){
                    deptEmpRepository.delete(deptEmp1);
                }
            }
            result = "Data has been deleted";
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_DELETE.getMessage());
        }
        return result;
    }
}
