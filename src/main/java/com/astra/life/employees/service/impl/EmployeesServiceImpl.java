package com.astra.life.employees.service.impl;

import com.astra.life.employees.enums.ResponseCode;
import com.astra.life.employees.enums.ResponseMessage;
import com.astra.life.employees.exception.RecordNotFoundException;
import com.astra.life.employees.mapper.EmployeeMapper;
import com.astra.life.employees.model.Employees;
import com.astra.life.employees.repository.EmployeesRepository;
import com.astra.life.employees.request.CreateEmployeeRequest;
import com.astra.life.employees.request.UpdateEmployeeRequest;
import com.astra.life.employees.response.EmployeesResponse;
import com.astra.life.employees.service.EmployeesService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class EmployeesServiceImpl implements EmployeesService {

    private final EmployeesRepository employeesRepository;
    private final EmployeeMapper employeeMapper;

    @Override
    public List<EmployeesResponse> getAllEmployee() throws RecordNotFoundException{
        List<EmployeesResponse> responseList = new ArrayList<>();
        try {
            Iterable<Employees> employees = employeesRepository.findAll();
            for(Employees employee : employees){
                responseList.add(employeeMapper.convertToResponse(employee));
            }
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return  responseList;
    }

    @Override
    public EmployeesResponse getEmployeeByEmpNo(Integer empNo) throws RecordNotFoundException{
        EmployeesResponse employeesResponse = new EmployeesResponse();
        try {
            Employees employees =  employeesRepository.findByEmpNo(empNo);
            employeesResponse = employeeMapper.convertToResponse(employees);
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), ResponseMessage.NOT_FOUND.getMessage());
        }
        return  employeesResponse;
    }

    @Override
    public EmployeesResponse create(CreateEmployeeRequest createEmployeeRequest) throws RecordNotFoundException {
        Employees employee = new Employees();
        EmployeesResponse response = new EmployeesResponse();
        employeeMapper.toModel(employee, createEmployeeRequest);

        LocalDate today = LocalDate.now();
        if(employee.getBirthDate().isAfter(today)) throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "BirthDate can't be more than current date");
        if(createEmployeeRequest.getFirstName().length() < 3) throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "Name must be longer more than 2 Characters");
        try{
            employee = employeesRepository.save(employee);
            response = employeeMapper.convertToResponse(employee);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }

    @Override
    public EmployeesResponse update(UpdateEmployeeRequest updateEmployeeRequest) throws RecordNotFoundException {
        Employees employee = new Employees();
        Employees emp = new Employees();
        EmployeesResponse response = new EmployeesResponse();
        employeeMapper.toModelUpdate(employee, updateEmployeeRequest);
        LocalDate today = LocalDate.now();
        if(employee.getBirthDate().isAfter(today)) throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "BirthDate can't be more than current date");
        if(updateEmployeeRequest.getFirstName().length() < 3) throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), "Name must be longer more than 2 Characters");
        try{
            employee = employeesRepository.save(employee);
            response = employeeMapper.convertToResponse(employee);

        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_SAVE_DATA.getMessage());
        }
        return response;
    }

    @Override
    public String delete(Integer empNo) throws  RecordNotFoundException{
        String result;
        try {
            Employees employees = employeesRepository.findByEmpNo(empNo);
            if (employees == null)throw new RecordNotFoundException(HttpStatus.NOT_FOUND, ResponseCode.NOT_FOUND.getCode(), "Employee not Found");
            employeesRepository.delete(employees);
            result = "Data has been deleted";
        }catch (Exception e){
            throw new RecordNotFoundException(HttpStatus.BAD_REQUEST, ResponseCode.BAD_REQUEST.getCode(), ResponseMessage.FAILED_TO_DELETE.getMessage());
        }
        return result;
    }


}
